# MANUEL D’UTILISATION

# INF3172: TRAVAIL PRATIQUE 2

## DESCRIPTION

Ce projet est une calculatrice scientifique à quatre fonctions qui utilise les mécanismes de communication interprocessus pour réaliser la tâche. Pour y arriver, le projet utilise une architecture client/serveur. 

D'un coté, il y a un programme clavier qui agit en tant que client. De l'autre coté, il y a un programme calculatrice qui agit en tant que serveur.

Ce travail est réalisé dans le câdre du cours: INF3172: Principes des 
systèmes d'exploitation, à la session d'hivers 2018.

Présenté à M. Ammar Hamad.

## AUTEURS

* Pier-Olivier Decoste (DECP09059005)
* Lou-Gomes Neto (NETL14039105)
* Hugo Twigg-Côté (TWIH25048700)

## SPÉCIFICATIONS IMPORTANTES

* Tous les commandes sont réalisés dans un terminal à la racine du projet. 
* Chaqun des programmes (Clavier et Calculatrice) doivent êtres exécutés dans un terminal différent.
* La calculatrice (serveur) doit etre en exécution en premier. Par la suite, les clients peuvent l'utilisier à l'aide du programme clavier (client).

## COMPILATION

La compilation peut se faire, dans un terminal, à l'aide de la commande :

```sh
make
```

## FONCTIONNEMENT

### Serveur

Une fois compilé, il faut lancer le programme serveur en premier à l'aide de la commande suivante:

```sh
./Calculatrice
```

Cette partie doit être faite, seulement si le serveur n'est pas déjà en exécution.

### Client

Pour chaque utilisateur, il faut lancer le programme client dans un terminal séparé à l'aide de la commande suivante:

```sh
./Clavier
```

Par la suite, il suffit de suivre les instructions à la ligne de commande.

Pour quitter, il suffit d'entrer le numéro `0` suivi de la fonction `FIN`. Pour fermer le serveur, il suffit d'entrer le numéro `0` suivi de la fonction `FINFIN`.

## RÉFÉRENCES

* Page des laboratoires du cours INF3172 groupe 10 hivers 2018. Le site comprend quelques exemples sur lesquels ce progamme est basé. (Google classroom)
* Documentation des séances du cours INF3172 groupe 10 hivers 2018. (moodle uqam)
* Ajout du fichier d'entete math.h pour les manipulations mathématiques.