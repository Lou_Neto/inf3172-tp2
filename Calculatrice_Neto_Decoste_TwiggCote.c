/**
 * Lou-Gomes Neto (NETL14039105)
 * Hugo Twigg-Côté (TWIH25048700)
 * Pier-Olivier Decoste (DECP09059005) 
 */ 
#include <stdio.h>
#include <stdlib.h>
#include <error.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#define CLIENT_EXIT_MSG    "Un client vient de quitter!"
#define SERVER_EXIT_MSG    "Merci d’utiliser le service de Calculatrice"
#define SERVER_FIFO        "/tmp/calculatrice_fifo_server"

enum exit_code {
    client_exit = 1,
    server_exit = 2,
};

// main -------------------------------------

int main(int argc, char **argv) {

    int fd, fd_client, bytes_read, error;
    char *numberStr, *function, *return_fifo, *ptr, buf[8191];
    double result, number, exit_status;

        // make server fifo
        if((mkfifo(SERVER_FIFO, 0664) == -1) && (errno != EEXIST)) {
            perror("Erreur: creation de tube pour le serveur");
            exit(1);
        }
        // open server
        if((fd = open(SERVER_FIFO, O_RDONLY)) == -1) {
            perror("Erreur: ouverture serveur");
            exit(1);
        }

        while(1) {
            // get request
            memset(buf, '\0', sizeof(buf));
            if((bytes_read = read(fd, buf, sizeof(buf))) == -1)
                perror("Erreur: lecture d'une requete");
            
            if(bytes_read == 0)
                continue;
            
            if(bytes_read > 0) {
                result = 0;
                number = 0;
                error = 0;
                exit_status = 0;

                // process request
                return_fifo = strtok(buf, ", \n");
                numberStr = strtok(NULL, ", \n");
                function = strtok(NULL, ", \n");
                number = strtod(numberStr, &ptr);

                if(*ptr) {
                    error += 1;
                }else{
                    if(strcmp(function, "SQR") == 0) {
                        result = number * number;
                    }else if(strcmp(function, "SQRT") == 0) {
                        result = sqrt(number);
                    }else if(strcmp(function, "SIN") == 0) {
                        result = sin(number);
                    }else if(strcmp(function, "COS") == 0) {
                        result = cos(number);
                    }else if((number == 0) && (strcmp(function, "FIN") == 0)) {
                        exit_status = client_exit;
                    }else if((number == 0) && (strcmp(function, "FINFIN") == 0)) {
                        exit_status = server_exit;
                    }else{
                        error += 1;
                    }
                }
            }

            // send result to client
            if ((fd_client = open(return_fifo, O_WRONLY)) == -1) { 
                perror ("Erreur: ouverture du tube client"); 
                continue; 
            } 
            if (error) 
                sprintf(buf, "Erreur: informations de la requete.\n"); 
            else 
                sprintf(buf, "Reponse = %.8g\n", result); 
            if (write(fd_client, buf, strlen (buf)) != strlen (buf)) 
                perror("Erreur: ecriture de la reponse"); 
            if (close(fd_client) == -1) 
                perror("Erreur: fermeture du tube client");

            // client exit, server exit or continue
            if(exit_status == client_exit) {
                unlink(return_fifo);
                puts(CLIENT_EXIT_MSG);
            }else if(exit_status == server_exit) {
                puts(SERVER_EXIT_MSG);
                break;
            }
        }

    return 0;
}
