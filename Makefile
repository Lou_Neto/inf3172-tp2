all: Clavier Calculatrice

Clavier: Clavier_Neto_Decoste_TwiggCote.c
	gcc -o Clavier Clavier_Neto_Decoste_TwiggCote.c

Calculatrice: Calculatrice_Neto_Decoste_TwiggCote.c
	gcc -o Calculatrice Calculatrice_Neto_Decoste_TwiggCote.c -lm

clean:
	rm -f Clavier
	rm -f Calculatrice
	rm -f *.o