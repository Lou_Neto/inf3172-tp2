/**
 * Lou-Gomes Neto (NETL14039105)
 * Hugo Twigg-Côté (TWIH25048700)
 * Pier-Olivier Decoste (DECP09059005) 
 */ 
#include <stdio.h>
#include <stdlib.h>
#include <error.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#define SERVER_FIFO             "/tmp/calculatrice_fifo_server"
#define NUM_PROMPT              "Entrez un chiffre (0 pour quitter): \ntp2> "
#define CALC_FUNCTIONS_PROMPT   "\
choisissez une fonction a appliquer: \n\
    - SQR: carre\n\
    - SQRT: racine carre\n\
    - SIN: sinus\n\
    - COS: cosinus\n\
    - FIN: fin des requetes/quitter (fermer client)\n\
    - FINFIN: fermer la calculatrice/quitter (fermer serveur)\n\
tp2> "

void removeNewLine(char *line) {
    int end = strlen(line)-1;
    if(line[end] == '\n')
        line[end] = '\0';
}

// main ------------------------------------

char client_fifo_name[128], 
    client_results[128], 
    buf1[512], 
    buf2[2048], 
    history_entry[8191], 
    numberStr[512], 
    function[512], 
    *ptr;

double number;

int main(int argc, char **argv) {
    
    int fd, fd_server, fd_resultats, bytes_read;

    // make client fifo
    sprintf(client_fifo_name, "/tmp/calc_client_fifo%ld", (long) getpid());
    if(mkfifo(client_fifo_name, 0664) == -1)
        perror("Erreur: creation de tube pour le client");
    
    // make client results file name
    sprintf(client_results, "/tmp/calc_resultats_client%ld", (long) getpid());

    while(1) {
        // input number
        printf(NUM_PROMPT);
        fgets(buf1, sizeof(buf1), stdin);
        strcpy(buf2, client_fifo_name);
        strcat(buf2, " ");
        strcat(buf2, buf1);

        // save number
        strcpy(numberStr, buf1);
        number = strtod(numberStr, &ptr);
        removeNewLine(numberStr);

        // input calculator function
        printf(CALC_FUNCTIONS_PROMPT);
        fgets(buf1, sizeof(buf1), stdin);
        strcat(buf2, " ");
        strcat(buf2, buf1);

        // save function
        strcpy(function, buf1);
        removeNewLine(function);

        // send information to server
        if((fd_server = open(SERVER_FIFO, O_WRONLY)) == -1) {
            perror("Erreur: ouverture de connection au serveur");
            break;
        }
        if(write(fd_server, buf2, strlen(buf2)) != strlen(buf2)) {
            perror("Erreur: ecriture au serveur");
            break;
        }
        if(close(fd_server) == -1) {
            perror("Erreur: fermeture");
            break;
        }

        // read server response 
        if ((fd = open (client_fifo_name, O_RDONLY)) == -1) 
			perror ("Erreur: ouverture tube client"); 
		memset (buf2, '\0', sizeof (buf2)); 
		if ((bytes_read = read (fd, buf2, sizeof (buf2))) == -1) 
			perror ("Erreur: lecture de la reponse"); 
		if (bytes_read > 0) { 
			printf ("%s\n", buf2); 
        } 
		if (close (fd) == -1) { 
			perror ("Erreur: fermeture du tube client"); 
			break;
        }

        // save request to a history entry
        strcpy(history_entry, numberStr);
        strcat(history_entry, ", ");
        strcat(history_entry, function);
        strcat(history_entry, ", ");
        strcat(history_entry, buf2);

        // add entry to history file
        if((fd_resultats = open(client_results, O_RDWR|O_CREAT|O_APPEND, 0666)) == -1)
            perror ("Erreur: ouverture du fichier resultat");
        if(write(fd_resultats, history_entry, strlen(history_entry)) 
            != strlen(history_entry))
            perror("Erreur: ecriture dans le fichier resultats");
        if(close(fd_resultats) == -1) {
            perror("Erreur: fermeture");
        }

        // Verify client exit, server exit or continue
        if(number == 0 && (strcmp(function, "FIN") == 0)) {
            printf("Historique des requetes:\n");
            printf("------------------------\n");
            if((fd_resultats = open(client_results, O_RDONLY)) == -1)
                perror ("Erreur: ouverture du fichier resultat");
            memset (buf2, '\0', sizeof (buf2));
            if((bytes_read = read (fd_resultats, buf2, sizeof (buf2))) == -1)
                perror("Erreur: lecture du fichier resultats");
            if (bytes_read > 0) { 
			    printf ("%s", buf2); 
            }
            if(close(fd_resultats) == -1) {
                perror("Erreur: fermeture");
            }
            break;
        }
        if(number == 0 && (strcmp(function, "FINFIN") == 0)) {
            unlink(client_results);
            printf("Merci d’utiliser le service de Calculatrice\n");
            break;
        }
    }
    return 0;
}